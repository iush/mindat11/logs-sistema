# apt-get install libcurl4-gnutls-dev
#install.packages("curl")
#install.packages("httr")
install.packages("elastic")
library('elastic')

connect(es_host = "melchor", es_path = "", es_user="elastic", es_pwd = "changeme", es_port = 9200, es_transport_schema  = "http")

plosdat <- system.file("examples", "plos_data.json", package = "elastic")
invisible(docs_bulk(plosdat))

Search(index = "plos", size = 1)$hits$hits

Search(index = "plos", type = "article", q = "antibody", size = 1)$hits$hits

docs_get(index = 'plos', type = 'article', id = 4)
