# ELK

## Runing FileBeat

```bash
docker pull docker.elastic.co/beats/filebeat:5.6.3
```

```bash
docker run --rm -it -v "$PWD"/filebeat/filebeat.yml:/usr/share/filebeat/filebeat.yml -v $PWD/filebeat/logs:/logs docker.elastic.co/beats/filebeat:5.6.3
```

## Running LogStash

### Pulling logstash from logstash

```bash
docker pull docker.elastic.co/logstash/logstash:5.6.3
```

### Testing a frist basic configuration:

```
input { stdin { } } output { stdout {} }
```

```bash
docker run --rm -it -v $PWD:/usr/share/logstash/pipeline  docker.elastic.co/logstash/logstash:5.6.3 /usr/share/logstash/bin/logstash -f /usr/share/logstash/pipeline/logstash.conf
```

```bash
docker run --rm -it -v $PWD/logstash.conf:/usr/share/logstash/pipeline/logstash.conf  docker.elastic.co/logstash/logstash:5.6.3
```

```bash
docker run --rm -it docker.elastic.co/logstash/logstash:5.6.3 logstash -e 'input { stdin { } } output { stdout { } }'
```

### Reading a log file

```bash
docker run --rm -it docker.elastic.co/logstash/logstash:5.6.3 logstash -e 'input { beats { hosts=>"0.0.0.0" port => "5043" } } output { stdout { } }'
```

```bash
docker run --rm -it -v $PWD:/usr/share/logstash/pipeline  docker.elastic.co/logstash/logstash:5.6.3
```

```bash
docker run --rm -it -v $PWD/basic_nmon:/usr/share/logstash/pipeline/ -v $PWD/basic_nmon/logs:/logs docker.elastic.co/logstash/logstash:5.6.3
```

```yml
beats { port => "5043" }
path => ["/logs/gaspar_170928_0000.nmon"]
~/Documents/Projects/IUSH/IUSH_archive/MINDAT11/basic_nmon/logs
```

## Running elasticsearch

```bash
docker run -p 9200:9200 -p 9300:9300 -e "ES_JAVA_OPTS=-Xms512m -Xmx512m" -e "discovery.type=single-node" docker.elastic.co/elasticsearch/elasticsearch:5.6.3
```

```bash
docker run -p 9200:9200 -p 9300:9300 -e "ES_JAVA_OPTS=-Xms512m -Xmx512m" -e "discovery.type=single-node" -v "$PWD"/elasticsearch/esdata1:/usr/share/elasticsearch/data docker.elastic.co/elasticsearch/elasticsearch:5.6.3
```

```yml
version: '2'
services:
  elasticsearch1:
    image: docker.elastic.co/elasticsearch/elasticsearch:5.6.3
    container_name: elasticsearch1
    environment:
      - cluster.name=docker-cluster
      - bootstrap.memory_lock=true
      - "ES_JAVA_OPTS=-Xms512m -Xmx512m"
    ulimits:
      memlock:
        soft: -1
        hard: -1
    mem_limit: 1g
    volumes:
      - esdata1:/usr/share/elasticsearch/data
    ports:
      - 9200:9200
    networks:
      - esnet
  elasticsearch2:
    image: docker.elastic.co/elasticsearch/elasticsearch:5.6.3
    environment:
      - cluster.name=docker-cluster
      - bootstrap.memory_lock=true
      - "ES_JAVA_OPTS=-Xms512m -Xmx512m"
      - "discovery.zen.ping.unicast.hosts=elasticsearch1"
    ulimits:
      memlock:
        soft: -1
        hard: -1
    mem_limit: 1g
    volumes:
      - esdata2:/usr/share/elasticsearch/data
    networks:
      - esnet

volumes:
  esdata1:
    driver: local
  esdata2:
    driver: local

networks:
  esnet:
```


```bash
curl -u elastic:changeme http://localhost:9200/_cluster/health
{"cluster_name":"docker-cluster","status":"red","timed_out":false,"number_of_nodes":1,"number_of_data_nodes":1,"active_primary_shards":5,"active_shards":5,"relocating_shards":0,"initializing_shards":0,"unassigned_shards":11,"delayed_unassigned_shards":0,"number_of_pending_tasks":0,"number_of_in_flight_fetch":0,"task_max_waiting_in_queue_millis":0,"active_shards_percent_as_number":31.25}
```

elasticsearch.url:       http://elasticsearch:9200
```bash
curl -u elastic:changeme http://elasticsearch:9200/_cluster/health
```

```bash
[elaasticsearch@elasticsearch ~]$ curl -u elastic:changeme 'http://elasticsearch:92lasticsearch@elasticsearch ~]$ curl -uelastic:changeme 'http://elasticsearchhealth status index                             uuid                   pri rep docs.count docs.deleted store.size pri.store.size
yellow open   .monitoring-kibana-6-2017.11.08   Q-4_phemQsiXr9LfqLjyZg   1   1         16            0    101.7kb        101.7kb
yellow open   .kibana                           5iIarPliRKeDq7UWGruN5Q   1   1          1            0      3.2kb          3.2kb
red    open   .watcher-history-6-2017.11.08     Lu-s64saT1KeV-t-y4hKtw   1   1
red    open   logstash-2017.11.09               pdgjh74qQFuelzdvOUhFNg   5   1       8283            0      9.6mb          9.6mb
red    open   .monitoring-es-6-2017.11.09       CuEpfkTeTwanSh8j8wzqDA   1   1
yellow open   .monitoring-alerts-6              YKUnxew5QCWgBXGj58fufw   1   1          1          180     24.8kb         24.8kb
red    open   .watches                          wwRO9dMpQLuTNlDfNIl4_A   1   1
yellow open   .monitoring-es-6-2017.11.08       Ly0JhhPBTdq_Va-riTN3Ww   1   1      15082           60      8.5mb          8.5mb
yellow open   .monitoring-logstash-6-2017.11.11 xrBUW1U0RPeXGX4epx08IA   1   1        220            0    132.9kb        132.9kb
red    open   .monitoring-logstash-6-2017.11.08 H-oQUicYS8Wd9MNwRKRXRA   1   1
yellow open   .triggered_watches                eC2dnA7MTq2wxvWksSqCgg   1   1          7          901    114.9kb        114.9kb
yellow open   .monitoring-es-6-2017.11.11       LEsKN6CjTPWdkDTHdXqcog   1   1       2443          160      1.7mb          1.7mb
red    open   .monitoring-logstash-6-2017.11.09 nItUp2EhQAe15wbQYER5ig   1   1
```
![image](images/kibana_index_pattern.png)

curl -XPUT 'elasticsearch:9200/_xpack/security/user/kibana/_password?pretty' -H 'Content-Type: application/json' -d'
{
  "password": "kibanapassword"
}
'
