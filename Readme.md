![Men Working](https://gitlab.com/iush/hardware-architecture/raw/master/images/menworking.png "Document under maintenance!")

<section>
<iframe  frameborder=0 width="630" height="170" marginheight=0 marginwidth=0 scrolling="auto" src="http://oems.com.co/demos/trex/index.html"></iframe>
</section>
[T-Rex](https://github.com/amaneureka/T-Rex/)

Warnings:
There are some attempts at humor here.
----

# Administration technological resources.

----

## 1. Problem definition

Make diagnoses and projections for server farms, require data analysis and processing of large amount of data, these processes can be not very efficient and usually do not have any support.

![Rack empty ](https://gitlab.com/mindat11/logs-sistema/raw/master/images/rack_empty.jpg)

----

### Consequences:

![Slow](https://gitlab.com/mindat11/logs-sistema/raw/master/images/slow.png)

- Problems in the operation due to delays in the processes derived from the use of the technological resource.
- Over-costs by over estimating the requirements of the technological resource.

----

### Causes:
The causes of incorrectly using the technological resource may be the lack of effective techniques in the estimation of current consumption.

----

### Goals:

Estimate and optimize the use of technological resources:

- Centralize logs of different types in a single tool that allows their analysis in a unified and immediate way.
- Automatically detect anomalies in the operation.
- Make predictions about the growth of the platform.
- Develop analysis techniques to optimize the use of the technological resource.

----

## 2. Selection of data sources for analysis

In general we will be based on consumption hystory, based on CPU, network, memory and storage consumption on Unix systems.

_
====

## Entry methods

- logstash
- metricbeat
- filebeat

_
====

## logstash
![Rack empty ](https://gitlab.com/mindat11/logs-sistema/raw/master/images/logstashErrpt.png)

_
====

### The next is a example of the log:

```logs
LABEL:		REPLACED_FRU
IDENTIFIER:	2F3E09A4

Date/Time:       Tue Apr  5 11:40:06 2016
Sequence Number: 1543600
Machine Id:      00XXXXXX4K00
Node Id:         vio1
Class:           H
Type:            INFO
WPAR:            Global
Resource Name:   fscsi4
Resource Class:  driver
Resource Type:   efscsi
Location:        UXXXX.001.9XXXXXX-P1-C5-T1


Description
REPAIR ACTION

User Causes
PARTS REPLACEMENT

	Recommended Actions
	NONE
```
_
====

### Example on the JSON output on logstash of previus errpt example.
[JSON](https://en.wikipedia.org/wiki/JSON)

```logs
{
				  "message" => ";__LN_BK__;LABEL:\t\tREPLACED_FRU;__LN_BK__;IDENTIFIER:\t2F3E09A4;__LN_BK__;;__LN_BK__;Date/Time:       Tue Apr  5 11:40:06 2016;__LN_BK__;Sequence Number: 1543600;__LN_BK__;Machine Id:      00XXXXXX4C00;__LN_BK__;Node Id:         vio1;__LN_BK__;Class:           H;__LN_BK__;Type:            INFO;__LN_BK__;WPAR:            Global;__LN_BK__;Resource Name:   fscsi4;__LN_BK__;Resource Class:  driver;__LN_BK__;Resource Type:   efscsi;__LN_BK__;Location:        UXXXX.001.XXXXXXN-P1-C5-T1;__LN_BK__;;__LN_BK__;;__LN_BK__;Description;__LN_BK__;REPAIR ACTION;__LN_BK__;;__LN_BK__;User Causes;__LN_BK__;PARTS REPLACEMENT;__LN_BK__;;__LN_BK__;\tRecommended Actions;__LN_BK__;\tNONE",
				 "@version" => "1",
			   "@timestamp" => "2016-06-14T21:13:19.864Z",
					 "beat" => {
		"hostname" => "oemunoz",
			"name" => "oemunoz"
	},
			   "input_type" => "log",
					"count" => 1,
				   "fields" => [],
				   "source" => "errpt.example",
					 "type" => "log",
					 "host" => "oemunoz",
					 "tags" => [
		[0] "beats_input_codec_plain_applied",
		[1] "multiline",
		[2] "errpt.log",
		[3] "errpt.basic"
	],
				  "e_label" => "REPLACED_FRU",
			 "e_identifier" => "2F3E09A4",
			   "e_datetime" => "Tue Apr  5 11:50:06 2016",
		"e_sequence_number" => "1543600",
			 "e_machine_id" => "00XXXXXX4C00",
				"e_node_id" => "vio1",
				  "e_class" => "H",
				   "e_type" => "I",
				   "e_wpar" => "Global",
		  "e_resource_name" => "fscsi4",
		 "e_resource_class" => "driver",
		  "e_resource_type" => "efscsi",
			   "e_location" => "UXXX3.001.XXXX1V",
			"e_description" => "REPAIR ACTION",
			"e_user_causes" => "PARTS REPLACEMENT",
	"e_recommended_actions" => "NONE"
}
```

## filebeat
![Rack empty ](https://gitlab.com/mindat11/logs-sistema/raw/master/images/logstashErrpt.png)

_
====

## metricbeat
![Rack empty ](https://gitlab.com/mindat11/logs-sistema/raw/master/images/logstashErrpt.png)

----

## 3. Identificación de variables de análisis

In particular, we will look for the relationship that the consumption of resources has with the server processes, seeking to infer user response times, number of users and long-term tasks on the servers.

_
====

![dance](https://gitlab.com/mindat11/logs-sistema/raw/master/images/cpu1.jpeg)

Ver cómo responden a los cambios las diferentes configuraciones, buscando entender el comportamiento de granja de servidores en cuanto a una serie de factores, incluidos:
- Cuántos usuarios simultáneos usan el sistema.
- Qué tipos de operaciones de usuario se realizan.
- Que tipo de procesos se ejecutan en el servidor

----

## 4. Determine el tipo de análisis y la posible técnica de minería de datos que pueda aplicar

Principalmente buscaremos hacer análisis de correlaciones para determinar como se relacionan los recursos entre ellos y luego como afectan a los procesos y en general varias vistas del comportamiento de la misma.

![Slow](https://gitlab.com/mindat11/logs-sistema/raw/master/images/correlations.jpeg)

También se buscara establecer mediante análisis de regresión si existe un comportamiento que permita estimar recursos en próximas implementaciones.

La usara la visualización de datos para dar significado a la estadística que se pueda desarrollar.

Se buscara determinar la posibilidad de usar técnicas de programación y optimización matemática para optimizar el uso de los recursos.

Por ultimo, el uso de redes neuronales y las técnicas de virtualización pueden permitir direccionar y administrar recursos automáticamente, de acuerdo a los comportamientos previos.
