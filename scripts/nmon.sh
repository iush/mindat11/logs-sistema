#!/bin/sh
#
# /etc/rc.d/rc.nmon
#
# Start/stop/restart
# the nmon process.
#
# To make Nmon start automatically at boot, make this
# file executable:  chmod 755 /etc/rc.d/rc.nmon
#
# Version 0.4
# which for nmon and gzip
OUTPUTDIR=/var/log/nmon
NMON=$(which nmon)
GZIP=$(which gzip)
nmon_start() {
        if [ -d $OUTPUTDIR ]; then
                echo "Directory exists"
        else
                echo "Directory does not exists, creating..."
                mkdir $OUTPUTDIR
        fi
        echo "nmon started whit log $OUTPUTDIR"
        $NMON -f -t -m $OUTPUTDIR
}
nmon_stop() {
        echo "killing nmon"
        PIDS=$(ps -ax 2>/dev/null | grep -v "grep" | grep "nmon -f -t" | awk '{ print $1 }')
        for PID in $PIDS; do
                kill $PID
        done
    $GZIP $OUTPUTDIR/*.nmon
}
case "$1" in
   'start')
        nmon_start
   ;;
   'stop')
       nmon_stop
   ;;
   'restart')
        nmon_stop 
        nmon_start
   ;;
   *)
       echo "Usage: $0 {start|stop|restart}"
   ;;
 esac
