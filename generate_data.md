# Generando datos del sistema

changeme
```bash
cat gaspar_171120_0000.nmon | grep TOP | more
```


# Conectando Elasticsearch con R

[ropensci](https://github.com/ropensci/elastic)

```R
connect(es_host = "melchor", es_path = "", es_user="elastic", es_pwd = "changeme", es_port = 9200, es_transport_schema  = "http")
transport:  http
host:       melchor
port:       9200
path:       NULL
username:   elastic
password:   <secret>
errors:     simple
headers (names):  NULL
```

```R
Search(index = "plos", size = 1)$hits$hits
Error: 404 - no such index
```

```R
shakespeare <- system.file("examples", "shakespeare_data.json", package = "elastic")
invisible(docs_bulk(shakespeare))
plosdat <- system.file("examples", "plos_data.json", package = "elastic")
invisible(docs_bulk(plosdat))
```

```R
> shakespeare
[1] "/home/oemunoz/R/x86_64-pc-linux-gnu-library/3.4/elastic/examples/shakespeare_data.json"
```
